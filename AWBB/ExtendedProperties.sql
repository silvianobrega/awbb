﻿EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AdventureWorks 2016 Sample OLTP Database';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Input parameter for the table value function ufnGetContactInformation. Enter a valid PersonID from the Person.Contact table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'FUNCTION', @level1name = N'ufnGetContactInformation', @level2type = N'PARAMETER', @level2name = N'@PersonID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Input parameter for the scalar function ufnGetDocumentStatusText. Enter a valid integer.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'FUNCTION', @level1name = N'ufnGetDocumentStatusText', @level2type = N'PARAMETER', @level2name = N'@Status';

